;**************************************************************************************************
;vsprintf.asm -   atlwin40
;**************************************************************************************************

  .686
  .xmm 
  .mmx
  .model flat, c

  option casemap:none   ;; Case sensitive
  option prologue:none  ;; Turn off function stack context- constructor
  option epilogue:none  ;; Turn off function stack context- destructor
  
;; Extern CRT API
wcstombs proto c :dword, :dword, :dword

FCM_MASK_LEFT_ALIGN equ (1 shl 0)
FCM_MASK_SIGN_BIT equ (1 shl 1)
FCM_MASK_SPACE_OR_NEG equ (1 shl 2)
FCM_MASK_OUTPUT_PREFIX equ (1 shl 3)
FCM_MASK_ZEROPAD equ (1 shl 4)
FCM_MASK_PRECISION equ (1 shl 5)

FSM_MASK_NEG equ (1 shl 6)
FSC_MASK_FORMAT_U equ (1 shl 7)
FSC_MASK_FORMAT_FZERO equ (1 shl 8)

FTOA_MAX_PRECISION equ (16)

FPU_X87_PSR_C3_MASK equ (04000h)
FPU_X87_PSR_C2_MASK equ (00400h)
FPU_X87_PSR_C1_MASK equ (00200h)
FPU_X87_PSR_C0_MASK equ (00100h)
  
;; Length settings
INT_LENGTH equ 32   ;; int 

CHAR_LENGTH equ 8   ;; char 
WCHAR_LENGTH equ 16 ;; wchar_t
SHORT_LENGTH equ 16 ;; short 
LONG_INT_LENGTH equ 32 ;; long int 
LONG_LONG_INT_LENGTH equ 64 ;; long long int 
INT_MAX_LENGTH equ 64 ;; intmax_t 
SIZE_MAX_LENGTH equ 32 ;; size_t 
PTRDIFF_LENGTH equ 32 ;; ptrdiff_t 

DOUBLE_LENGTH equ 64 ;; double
FLOAT_LENGTH equ 32 ;; float
LONG_DOUBLE_LENGTH equ 64 ;; long double
REAL_LENGTH equ 80 ;; real 

PTR_LENGTH equ 32


SYMBOL  equ public 

  .code

vaargsr macro arg1, arglist:vararg
  local args
  args textequ <arg1>

  for arg, <arglist>
    args catstr <arg>, <!,>, args
  endm
  
  exitm <args>
endm 

$ccall_internal macro procedure, arglist:vararg 

  arg_n = 0
  
  for arg, <arglist>
    push arg 
    
    arg_n = arg_n+ 4
  endm 
  
  call procedure
  add  esp, arg_n
endm

$ccall macro procedure, arglist:vararg 

  %$ccall_internal procedure, vaargsr (arglist)
endm

$stack macro arglist:vararg 
  
  args textequ <![>
  arg_n = 0
  
  for arg, <arglist>
    if arg_n ne 0 
      args catstr args, <!+>, <arg>
    else 
      args catstr args, <esp!+>, <arg>, < >
    endif 
   
    arg_n = arg_n+ 4
  endm 
  
  args catstr args, <!]>
  
  exitm <args>
endm 

$push macro arglist:vararg
  
  arg_n = 0
  
  for arg, <arglist>
    push arg 
   
    arg_n = arg_n+ 4
  endm 
  
  exitm %arg_n
endm

$pop_internal macro arglist:vararg
  for arg, <arglist>
    pop arg 
  endm 
endm

$pop macro arglist:vararg
  
  arg_n = 0
  
  for arg, <arglist>
    arg_n = arg_n+ 4
  endm 
  
  %$pop_internal vaargsr (arglist)
  
  exitm %arg_n
endm


HIDWORD macro var 
  
  var2 catstr <4>, var
  exitm var2
endm 

LODWORD macro var 
  
  exitm var
endm 

;; ------------------------------------------------------------------------------------
;; Source from ..\VC\crt\src\intel.ulldvrm.asm 
;; ------------------------------------------------------------------------------------
;  EDX:EAX contains the quotient (dividend/divisor)
;  EBX:ECX contains the remainder (divided % divisor)
;  NOTE: this routine removes the parameters from the stack.
ulldiv proc near private

    push    esi

DVND    equ     [esp + 8]       ; stack address of dividend (a)
DVSR    equ     [esp + 16]      ; stack address of divisor (b)

    mov     eax,HIDWORD(DVSR) ; check to see if divisor < 4194304K
    or      eax,eax
    jnz     short L1        ; nope, gotta do this the hard way
    mov     ecx,LODWORD(DVSR) ; load divisor
    mov     eax,HIDWORD(DVND) ; load high word of dividend
    xor     edx,edx
    div     ecx             ; get high order bits of quotient
    mov     ebx,eax         ; save high bits of quotient
    mov     eax,LODWORD(DVND) ; edx:eax <- remainder:lo word of dividend
    div     ecx             ; get low order bits of quotient
    mov     esi,eax         ; ebx:esi <- quotient

    mov     eax,ebx         ; set up high word of quotient
    mul     dword ptr LODWORD(DVSR) ; HIDWORD(QUOT) * DVSR
    mov     ecx,eax         ; save the result in ecx
    mov     eax,esi         ; set up low word of quotient
    mul     dword ptr LODWORD(DVSR) ; LODWORD(QUOT) * DVSR
    add     edx,ecx         ; EDX:EAX = QUOT * DVSR
    jmp     short L2        ; complete remainder calculation

L1:
    mov     ecx,eax         ; ecx:ebx <- divisor
    mov     ebx,LODWORD(DVSR)
    mov     edx,HIDWORD(DVND) ; edx:eax <- dividend
    mov     eax,LODWORD(DVND)
L3:
    shr     ecx,1           ; shift divisor right one bit; hi bit <- 0
    rcr     ebx,1
    shr     edx,1           ; shift dividend right one bit; hi bit <- 0
    rcr     eax,1
    or      ecx,ecx
    jnz     short L3        ; loop until divisor < 4194304K
    div     ebx             ; now divide, ignore remainder
    mov     esi,eax         ; save quotient

    mul     dword ptr HIDWORD(DVSR) ; QUOT * HIDWORD(DVSR)
    mov     ecx,eax
    mov     eax,LODWORD(DVSR)
    mul     esi             ; QUOT * LODWORD(DVSR)
    add     edx,ecx         ; EDX:EAX = QUOT * DVSR
    jc      short L4        ; carry means Quotient is off by 1

    cmp     edx,HIDWORD(DVND) ; compare hi words of result and original
    ja      short L4        ; if result > original, do subtract
    jb      short L5        ; if result < original, we are ok
    cmp     eax,LODWORD(DVND) ; hi words are equal, compare lo words
    jbe     short L5        ; if less or equal we are ok, else subtract
L4:
    dec     esi             ; subtract 1 from quotient
    sub     eax,LODWORD(DVSR) ; subtract divisor from result
    sbb     edx,HIDWORD(DVSR)
L5:
    xor     ebx,ebx         ; ebx:esi <- quotient

L2:
    sub     eax,LODWORD(DVND) ; subtract dividend from result
    sbb     edx,HIDWORD(DVND)
    neg     edx             ; otherwise, negate the result
    neg     eax
    sbb     edx,0

    mov     ecx,edx
    mov     edx,ebx
    mov     ebx,ecx
    mov     ecx,eax
    mov     eax,esi

    pop     esi
    ret    

ulldiv ENDP

;; bool 
isdigit proc near private

  movzx ecx, byte ptr[esp+4]
  cmp   ecx, '0' 
  sbb   eax, eax 
  not   eax 
  sub   ecx, ('9' + 1)
  and   eax, ecx 
  shr   eax, 31 
  ret   
  
isdigit endp 

;; not set tail buffer "NULL"
;; return <- render buffer count 
;; 
;; eax <- int result 
;; ecx <- buffer seek 
;;
;; @DEBUG
xxxatoi proc near private
 
ARG_BUFFER = 4 
 
  stk_push = $push (ebx, ebp, esi, edi)
  
  mov   ebp, $stack (stk_push, ARG_BUFFER)   ;; buffer 
  xor   edi, edi  ;; Result 
  xor   esi, ebp  ;; Seek 
  xor   ebx, ebx  ;; Sign 
  
  test  ebp, ebp 
  je    @exit
  
  ;; Collect '-', '+'
@@:
  movzx eax, byte ptr[ebp]
  test  eax, eax 
  je    @exitp  
  
  xor   ecx, ecx 
  
  cmp   eax, '+'
  mov   edx, 1 
  cmove ecx, edx 
  
  cmp   eax, '-'
  mov   edx, 2
  cmove ecx, edx 
  
  test  ecx, ecx 
  je    @zeropad
  
  add   ebp, 1 
  mov   ebx, ecx  ;; 1:+ 2:- 
  
  shr   ecx, 1    ;; 0:+ 1:- 

  jmp   @B 
 
@zeropad:
  ;; Collect prefix zero e.g. 000112 or -001222444
  
  movzx eax, byte ptr[ebp]
  test  eax, eax 
  je    @exitp 
  
  xor   ecx, ecx 
  
  cmp   eax, '0'
  jne   @buf2intp
  
  add   ebp, 1 
  jmp   @B 

@buf2intp:

  movzx eax, byte ptr[ebp]
  test  eax, eax 
  je    @exitp 
  
  ;; Test first byte is digit
  $ccall isdigit, eax 
  
  test  eax, eax 
  je    @exitp
  
  mov   eax, ebp 
  sub   eax, esi 
  
  xor   esi, esi 
@@:
  ;; Is digit && ch != NULL ??
  movzx eax, byte ptr[ebp]
  mov   ebx, eax 
  cmp   eax, '0'
  sbb   ecx, ecx 
  not   ecx 
  sub   eax, ('9' + 1)
  and   ecx, eax 
  mov   eax, ebx 
  sub   eax, 1
  not   eax 
  and   ecx, eax 
  shl   ecx, 1 
  jnc   @exit      ;; XXX:Too simple..
  
  add   esi, 1 
  add   ebp, 1
  
  ;; Result = base * 10 + (ch - '0')
  lea   edi, [edi * 4 + edi]
  lea   edi, [edi * 2 + ebx - '0'] 
  
  jmp   @B 

@exitp:
  xor   esi, esi 
@exit:
  mov   eax, edi 
  mov   ecx, esi 
  
  stk_pop = $pop  (ebx, ebp, esi, edi)
  
  ret   
  
xxxatoi endp 


fpush_make macro max
  idx = 0
  
  t catstr <0>, <.>
  
  while idx ne max 
   t catstr t, <0>
   
   idx = idx + 1
  endm 
  
  t catstr t, <5>
  
  %echo t  ;;; For debug ..
  
  exitm t

endm 
 
flut_make macro var_name, max

  vec = 0
  vec_max = max + 1
  
  m_entries catstr <var_name>, < tbyte >
   
  while vec ne vec_max 
    if (vec mod 16  eq 0) and (vec ne 0)
      m_entries catstr < tbyte > 
    endif 
    if vec mod 16 ne 15
      if vec eq (vec_max - 1) 
        m_entries catstr m_entries, fpush_make (vec)
        m_entries
      else 
        m_entries catstr m_entries, fpush_make (vec), <!, >
      endif 
    else   
      m_entries catstr m_entries, fpush_make (vec)
      m_entries
    endif 
    vec = vec + 1
  endm 
endm 


.const


 ;; Make float push table 
 
 flut_make ft_precision_push_lut, FTOA_MAX_PRECISION

 ft_1      tbyte 1.0 
 ft_10     tbyte 10.0 
 ft_100    tbyte 100.0 
 ft_1000   tbyte 1000.0 
 ft_10000  tbyte 10000.0
 ft_100000 tbyte 100000.0
 
.code 

;; ------------------------------------------------------------
format$d proc near SYMBOL     
    
  ;; Process format %d, %i  
  
  ;; buffer:[esp+4]
  ;; flags:[esp+8]
  ;; width:[esp+12]
  ;; precision:[esp+16]
  ;; 
  ;; length_size:[esp+20]
  ;; value-lo[esp+24]
  ;; value-hi[esp+28] (only for int 64)
  
  ;; ret:use buffer length 
  
ARG_BUFFER = 4 
ARG_FLAGS = 8 
ARG_WIDTH = 12 
ARG_PRECISION = 16 
ARG_LENGTH = 20
ARG_VALUE_LO = 24 
ARG_VALUE_HI = 28 

  stk_push = $push (ebx, ebp, esi, edi)
  
  mov   edi, $stack (stk_push, ARG_BUFFER)  
  mov   ebp, $stack (stk_push, ARG_FLAGS) 
         
  mov   ecx, $stack (stk_push, ARG_LENGTH) 
  nop        
              
  mov   eax, $stack (stk_push, ARG_VALUE_LO) 
  mov   edx, $stack (stk_push, ARG_VALUE_HI) 
  
  cmp   ecx, 32 
  jg    @F 
 
  ;;     ecx  free (??????????)
  ;;     ebx  free 
  ;;     esi  free 
  sub   ecx,   1   ;; msb bit 31 or 15 or 7 
  mov   ebx, 0ffffffffh  
  
  bt    eax, ecx
  sbb   esi, esi   ;; 00000000(msb miss) or ffffffff(msb hit)
  
  shl   esi, cl  ;;   ffffff80 ffff8000 80000000 
                  ;;or 00000000 00000000 00000000  
  
  shl   ebx, cl  ;;   ffffff80 ffff8000 80000000  
  not   ebx      ;;    0000007f 00007fff 7fffffff
  
  and   eax, ebx ;; clear other bit and sign bit 
  
  or    eax, esi ;; or 0 or neg mask 
  cdq             ;; to qword sign 
  
@@:
  ;;     ecx  free
  ;;     ebx  free 
  ;;     esi  free 
  
  ;;     save buffer base ptr 
  mov   esi,  edi 
  
  ;;     ecx  free
  ;;     ebx  free 
  
  ;;  Sign bit setting will cancel out the FCM_MASK_SPACE_OR_NEG mask
  mov   ecx, ebp
  and   ecx, not (FCM_MASK_SPACE_OR_NEG)
  test  ebp, FCM_MASK_SIGN_BIT 
  cmovne ebp, ecx 
  
  ;;  Precision setting will cancel out the FCM_MASK_ZEROPAD mask
  mov   ecx, ebp
  and   ecx, not (FCM_MASK_ZEROPAD)
  test  ebp, FCM_MASK_PRECISION 
  cmovne ebp, ecx 
  
  ;; The FCM_MASK_LEFT_ALIGN mask conflicts with the FCM_MASK_ZEROPAD mask, remove FCM_MASK_ZEROPAD
  mov   ecx, ebp
  and   ecx, not (FCM_MASK_ZEROPAD)
  test  ebp, FCM_MASK_LEFT_ALIGN 
  cmovne ebp, ecx 

  test  edx,  (1 shl 31)
  je    @F 
  
  ;; Remove sign bit 
  neg   eax 
  adc   edx, 0
  neg   edx 

  ;; Add sign bit mask 
  or    ebp,      FSM_MASK_NEG
  ;; Remove flags::plus 
  and   ebp, not FCM_MASK_SIGN_BIT 
  ;; Remove flags::space 
  and   ebp, not FCM_MASK_SPACE_OR_NEG

@@:
  ;; Process int8/int16/int32 format 

  mov   ecx, eax 
  or    ecx, edx 
  
  je    @F 

  ;;  Get div result and mod
  $ccall  ulldiv, eax, edx, 10, 0
  
  add   cl, '0'
  
  ;; Write buffer 
  mov   [edi], cl 
  add   edi,  1 
  
  jmp   @B 
  
@@:
  mov   eax,  $stack (stk_push, ARG_PRECISION) ;; Precision
  
  mov   edx,   edi 
  
  sub   edx,   esi   ;; Get render buffer count 
  sub   eax,   edx 
  jle   @F 
  
  ;;    Precision push zero pad .
@prec_pad:
  mov   byte ptr[edi], '0'
  add   edi,   1
  dec   eax 
  jne   @prec_pad
  
@@:
  ;; Check left space pad 
  test  ebp,   FCM_MASK_LEFT_ALIGN 
  jne   @sign_m 
  
  xor   eax,  eax
  mov   ecx,  1 
  
  test  ebp, (FSM_MASK_NEG or FCM_MASK_SIGN_BIT or FCM_MASK_SPACE_OR_NEG)
  cmovne eax,  ecx 
  
  mov   ebx,   $stack (stk_push, ARG_WIDTH);; width 
  lea   edx,   [edi+eax]
  
  sub   edx,   esi  ;; Get render buffer count 
  sub   ebx,   edx 
  jle   @sign_m 
  
  test  ebp,  FCM_MASK_ZEROPAD 
  jne   @lzero_pad 

  ;; If pad is '0', normal stutf 
  ;; If pad is ' ', advance a char pos write buffer 
  
  ;; e.g. 
  ;; 
  ;; -00062                   
  ;; -                   00062 (error)
  ;; -000000000000000000000062
  ;;  
  ;;                    -00062 (right)
  mov   edx,   edi
  
@lspace_pad:
  mov   byte ptr[edi+1], ' '
  add   edi,   1
  dec   ebx 
  jne   @lspace_pad 
  
  ;; Check append '-' or '+' or ' '
  test  ebp, (FSM_MASK_NEG or FCM_MASK_SIGN_BIT or FCM_MASK_SPACE_OR_NEG)
  je    @F 
  
  mov   ebx,  ' '
  mov   eax,  '+'
  mov   ecx,  '-'
  
  test  ebp, FCM_MASK_SIGN_BIT ;; High priority
  cmovne ebx,  eax
  
  test  ebp, FSM_MASK_NEG ;; Highest priority
  cmovne ebx,  ecx

  mov   byte ptr[edx], bl
  add   edi, 1
  
  jmp   @F 
  
@lzero_pad:
  mov   byte ptr[edi], '0'
  add   edi,   1
  dec   ebx 
  jne   @lzero_pad 
  
@sign_m:
  ;; Check append '-' or '+' or ' '
  test  ebp, (FSM_MASK_NEG or FCM_MASK_SIGN_BIT or FCM_MASK_SPACE_OR_NEG)
  je    @F 
  
  mov   ebx,  ' '
  mov   eax,  '+'
  mov   ecx,  '-'
  
  test  ebp, FCM_MASK_SIGN_BIT ;; High priority
  cmovne ebx,  eax
  
  test  ebp, FSM_MASK_NEG ;; Highest priority
  cmovne ebx,  ecx

  mov   byte ptr[edi], bl
  add   edi, 1

@@:
  ;; Reverse buffer ch 
  mov   eax, edi 
  sub   eax, esi ;; count 

  mov   ecx, eax 
  shr   ecx, 1 
  
  test  ecx, ecx 
  je    @F 
  
  mov   ebx, esi 
  lea   edx, [esi + eax - 1]

@reverse_ch:
  mov   al,  [edx]
  mov   ah,  [ebx]
  
  mov   [ebx], al 
  mov   [edx], ah 
  
  add   ebx, 1 
  sub   edx, 1 
  
  sub   ecx, 1
  jne   @reverse_ch
  
  ;; Check right space pad 
@@:
  test  ebp,   FCM_MASK_LEFT_ALIGN 
  je    exit 
  
  mov   eax, edi 
  sub   eax, esi ;; count 

  mov   ebx, $stack (stk_push, ARG_WIDTH) ;; width 
  sub   ebx, eax 
  
  jle   exit 
  
@rspace_pad:
  mov   byte ptr[edi], ' '
  add   edi,   1
  dec   ebx 
  jne   @rspace_pad 
  
exit:
  mov   eax, edi 
  sub   eax, esi 
  
  stk_pop = $pop (ebx, ebp, esi, edi)
  
  ret    
  
format$d endp

;; ------------------------------------------------------------
format$u proc near SYMBOL     
    
  ;; Process format %u
  
  ;; buffer:[esp+4]
  ;; flags:[esp+8]
  ;; width:[esp+12]
  ;; precision:[esp+16]
  ;; 
  ;; length_size:[esp+20]
  ;; value-lo[esp+24]
  ;; value-hi[esp+28] (only for unsigned int 64)
  
  ;; ret:use buffer length 
ARG_BUFFER = 4 
ARG_FLAGS = 8 
ARG_WIDTH = 12 
ARG_PRECISION = 16 
ARG_LENGTH = 20
ARG_VALUE_LO = 24 
ARG_VALUE_HI = 28 

  stk_push = $push (ebx, ebp, esi, edi)
  
  mov   edi, $stack (stk_push, ARG_BUFFER) 
  mov   ebp, $stack (stk_push, ARG_FLAGS) 

  mov   ecx, $stack (stk_push, ARG_LENGTH) 
  nop         
              
  mov   eax, $stack (stk_push, ARG_VALUE_LO)
  mov   edx, $stack (stk_push, ARG_VALUE_HI)
  
  ;; For %u, only these two flag bits can work effectively
  and   ebp, (FCM_MASK_ZEROPAD or FCM_MASK_LEFT_ALIGN)
  
  cmp   ecx, 32 
  jg    @F 
  
  ;;     ecx  free (??????????)
  ;;     ebx  free 
  ;;     esi  free 
  sub   ecx,   1   ;; msb bit 31 or 15 or 7 
  mov   ebx,   0ffffffffh  
  
  ;;   TODO: shl 8, 16,, 32 ????
  shl   ebx, cl  ;;   ffffff80 ffff8000 80000000  
  shl   ebx, 1    ;;   ffffff00 ffff0000 00000000  

  not   ebx      ;;    000000ff 0000ffff ffffffff
  
  and   eax, ebx ;;
  xor   edx, edx 
  
@@:
  ;;     ecx  free
  ;;     ebx  free 
  ;;     esi  free 
  
  ;;     save buffer base ptr 
  mov   esi,  edi 
  
  ;;     ecx  free
  ;;     ebx  free 
  
@@:
  ;; Process int8/int16/int32 format 

  mov   ecx, eax 
  or    ecx, edx 
  
  je    @F 

  ;;  Get div result and mod
  $ccall  ulldiv, eax, edx, 10, 0
  
  add   cl, '0'
  
  ;; Write buffer 
  mov   [edi], cl 
  add   edi,  1 
  
  jmp   @B 
  
@@:
  mov   eax,  $stack (stk_push, ARG_PRECISION) ;; Precision
  
  mov   edx,   edi 
  
  sub   edx,   esi   ;; Get render buffer count 
  sub   eax,   edx 
  jle   @F 
  
  ;;    Precision push zero pad .
pzero_pad:
  mov   byte ptr[edi], '0'
  add   edi,   1
  dec   eax 
  jne   pzero_pad
  
@@:
  ;; Check left space pad 
  test  ebp,   FCM_MASK_LEFT_ALIGN 
  jne   @F  
  
  mov   ebx,   $stack (stk_push, ARG_WIDTH);; width 
  lea   edx,   [edi+0]
  
  sub   edx,   esi  ;; Get render buffer count 
  sub   ebx,   edx 
  jle   @F 
  
  mov   eax,  ' '
  test  ebp,  FCM_MASK_ZEROPAD 
  mov   ecx,  '0'
  cmovne eax,  ecx 
  
lleft_pad:
  mov   byte ptr[edi], al
  add   edi,   1
  dec   ebx 
  jne   lleft_pad
  
@@:
  ;; Reverse buffer ch 
  mov   eax, edi 
  sub   eax, esi ;; count 

  mov   ecx, eax 
  shr   ecx, 1 
  
  test  ecx, ecx 
  je    @F 
  
  mov   ebx, esi 
  lea   edx, [esi + eax - 1]

@reverse_ch:
  mov   al,  [edx]
  mov   ah,  [ebx]
  
  mov   [ebx], al 
  mov   [edx], ah 
  
  add   ebx, 1 
  sub   edx, 1 
  
  sub   ecx, 1
  jne   @reverse_ch
  
  ;; Check right space pad 
@@:
  test  ebp,   FCM_MASK_LEFT_ALIGN 
  je    exit 
  
  mov   eax, edi 
  sub   eax, esi ;; count 

  mov   ebx, $stack (stk_push, ARG_WIDTH);; width 
  sub   ebx, eax 
  
  jle   exit 
  
@rspace_pad:
  mov   byte ptr[edi], ' '
  add   edi,   1
  dec   ebx 
  jne   @rspace_pad 
  
exit:
  mov   eax, edi 
  sub   eax, esi 
  
  stk_pop = $pop (ebx, ebp, esi, edi)
  
  ret    
  
format$u endp

;; ------------------------------------------------------------
format$x_$o_$b proc near SYMBOL
    
  ;; Process format %x, %X, %o, %b
  
  ;; %x:Hexadecimal lowercase mode e.g. (1233ea)
  ;; %X:Hexadecimal uppercase mode e.g. (555FD)
  ;; %o:Octal mode e.g. (777)
  ;; %b:Binary mode e.g. (010101..........)
  ;; ..... 
  ;; ..... EOF
  ;; 
  ;; buffer:[esp+4]
  ;; flags:[esp+8]
  ;; width:[esp+12]
  ;; precision:[esp+16]
  ;; 
  ;; length_size:[esp+20]
  ;; value-lo[esp+24]
  ;; value-hi[esp+28] (only for unsigned int 64)
  ;; 
  ;; base[esp+32] 
  ;;
  ;; uppercase[esp+36] (for a-z A-Z....)
  ;; 
  ;; prefix[esp+40] 
  
  ;; ret:use buffer length 
  
ARG_BUFFER = 4 
ARG_FLAGS = 8 
ARG_WIDTH = 12 
ARG_PRECISION = 16 
ARG_LENGTH = 20
ARG_VALUE_LO = 24 
ARG_VALUE_HI = 28 
ARG_BASE = 32 
ARG_UPPERCASE = 36 
ARG_PREFIX = 40

  stk_push = $push (ebx, ebp, esi, edi)
  
  mov   edi, $stack (stk_push, ARG_BUFFER) 
  mov   ebp, $stack (stk_push, ARG_FLAGS) 

  mov   ecx, $stack (stk_push, ARG_LENGTH) 
  nop         
              
  mov   eax, $stack (stk_push, ARG_VALUE_LO)
  mov   edx, $stack (stk_push, ARG_VALUE_HI)
  
  ;; For %x, %X, %o, %b, only these three flag bits can work effectively
  and   ebp, (FCM_MASK_ZEROPAD or FCM_MASK_LEFT_ALIGN or FCM_MASK_OUTPUT_PREFIX)
  nop  
  
  cmp   ecx, 32 
  jg    @F 
  
  ;;     ecx  free (??????????)
  ;;     ebx  free 
  ;;     esi  free 
  sub   ecx,   1   ;; msb bit 31 or 15 or 7 
  mov   ebx,   0ffffffffh  
  
  shl   ebx, cl  ;;   ffffff80 ffff8000 80000000  
  shl   ebx, 1    ;;   ffffff00 ffff0000 00000000  

  not   ebx      ;;    000000ff 0000ffff ffffffff
  
  and   eax, ebx ;;
  xor   edx, edx 
  
@@:
  ;;     ecx  free
  ;;     ebx  free 
  ;;     esi  free 
  
  ;;     save buffer base ptr 
  mov   esi,  edi 
  
  ;;     ecx  free
  ;;     ebx  free 
  stk_push2 = stk_push + $push (ebp)
  
  mov   ebp, $stack (stk_push2, ARG_UPPERCASE)  ;; is_uppercase ????
  
  test  ebp, ebp 
  mov   esi, 'a' 
  mov   ebp, 'A'
  cmovne esi, ebp
  sub   esi, 10 
  sub   esi, '0'
  
  mov   ebp, $stack (stk_push2, ARG_BASE)  ;; base ()
  
@@:
  ;; Process %x, %X, %o, %b format 

  mov   ecx, eax 
  or    ecx, edx 
  
  je    @F 

  ;;  Get div result and mod
  mov   ebp, $stack (stk_push2, ARG_BASE) ;; base

  $ccall  ulldiv, eax, edx, ebp, 0
  
  cmp   ecx, 10 
  sbb   ebp, ebp

  not   ebp 
  and   ebp, esi 

  lea   ecx, [ecx + ebp + '0']
  
  ;; Write buffer 
  mov   [edi], cl 
  add   edi,  1 
  
  jmp   @B 
  
@@:
  stk_pop2 = $pop (ebp)
  
  mov   eax,  [esp+stk_push+16] ;; Precision
  
  mov   edx,   edi 
  
  sub   edx,   esi   ;; Get render buffer count 
  sub   eax,   edx 
  jle   @F 
  
  ;;    Precision push zero pad .
@prec_pad:
  mov   byte ptr[edi], '0'
  add   edi,   1
  dec   eax 
  jne   @prec_pad
  
@@:
  ;; ---------------------------------------
  xor   eax,  eax   ;; Prefix length 
  
  ;; Check left space pad 
  test  ebp,   FCM_MASK_LEFT_ALIGN 
  jne   @prefix_m 

  ;; Check append prefix string 
  test  ebp,  FCM_MASK_OUTPUT_PREFIX 
  je    @F 
  
  mov   ebx,  $stack (stk_push, ARG_PREFIX)  ;; prefix 
  nop 
  
  test  ebx,  ebx 
  je    @F 
  
  movzx ecx,  byte ptr[ebx] 
  nop 
  
  test  ecx,  ecx 
  je    @F 
  
@prelen:
  movzx ecx,  byte ptr[ebx]
  test  ecx,  ecx 
  mov   edx,  edi  ;; Save base buffer ptr
  je    @F 
  
  add   eax,   1 
  add   ebx,   1 
  jmp   @prelen
  
  ;;     eax,  prefix count  
@@:
  mov   ebx, $stack (stk_push, ARG_WIDTH);; width 
  lea   edx,   [edi+eax]
  
  mov   esi,  $stack (stk_push, ARG_BUFFER)
  nop 
  
  sub   edx,   esi  ;; Get render buffer count 
  sub   ebx,   edx 
  jle   @prefix_m 
  
  test  ebp,  FCM_MASK_ZEROPAD 
  jne   @lzero_pad 
  
  ;; If pad is '0', normal stutf 
  ;; If pad is ' ', advance prefix pos write buffer 
  
  ;; e.g. 
  ;; 
  ;; 0x00062                   
  ;; 0x                   00062 (error)
  ;; 0x000000000000000000000062
  ;;  
  ;;                    0x00062 (right)
  mov   edx,  edi  ;; Save base buffer ptr
  
@lspace_pad:
  mov   byte ptr[edi+eax], ' '
  add   edi,   1
  dec   ebx 
  jne   @lspace_pad 
  
  ;; Check copy prefix 
@precpyp:
  test  eax, eax
  je    @F 
  
  mov   ebx,  $stack (stk_push, ARG_PREFIX)  ;; prefix 
  lea   ebx,  [ebx+eax-1] ;; To pointer buffer end 
  
  add   edi,  eax   ;; Add prefix buffer count 
  
@precpy:
  mov   cl,   byte ptr[ebx]
  mov   [edx], cl 

  sub   ebx,  1 
  add   edx,  1
  sub   eax,  1
  jne   @precpy
  
  jmp   @F 
  
@lzero_pad:
  mov   byte ptr[edi], '0'
  add   edi,   1
  dec   ebx 
  jne   @lzero_pad 

@prefix_m:
  ;; Check append prefix string 
  test  ebp,  FCM_MASK_OUTPUT_PREFIX 
  je    @F 
  
  mov   ebx,  $stack (stk_push, ARG_PREFIX)  ;; prefix 
  nop 
  
  test  ebx,  ebx 
  je    @F 
  
  movzx ecx,  byte ptr[ebx] 
  nop 
  
  test  ecx,  ecx 
  je    @F 
  
  xor   eax,  eax
@prelen2:
  movzx ecx,  byte ptr[ebx]
  test  ecx,  ecx 
  je    @precpy2p 
  
  add   eax,   1 
  add   ebx,   1 
  jmp   @prelen2

  ;; Check copy prefix 
  test  eax, eax
  je    @F 
@precpy2p:
  mov   ebx,  $stack (stk_push, ARG_PREFIX)  ;; prefix 
  lea   ebx,  [ebx+eax-1] ;; To pointer buffer end 
  
@precpy2:
  mov   cl,   byte ptr[ebx]
  mov   [edi], cl 

  sub   ebx,  1 
  add   edi,  1
  sub   eax,  1
  jne   @precpy2
  
@@:
  ;; Reverse buffer ch 
  mov   esi,  $stack (stk_push, ARG_BUFFER)
  nop 
  
  mov   eax, edi 
  sub   eax, esi ;; count 

  mov   ecx, eax 
  shr   ecx, 1 
  
  test  ecx, ecx 
  je    @F 
  
  mov   ebx, esi 
  lea   edx, [esi + eax - 1]

@reverse_ch:
  mov   al,  [edx]
  mov   ah,  [ebx]
  
  mov   [ebx], al 
  mov   [edx], ah 
  
  add   ebx, 1 
  sub   edx, 1 
  
  sub   ecx, 1
  jne   @reverse_ch
  
  ;; Check right space pad 
@@:
  test  ebp,   FCM_MASK_LEFT_ALIGN 
  je    exit 
  
  mov   esi,  $stack (stk_push, ARG_BUFFER)
  nop 
  
  mov   eax, edi 
  sub   eax, esi ;; count 

  mov   ebx, $stack (stk_push, ARG_WIDTH);; width 
  sub   ebx, eax 
  
  jle   exit 
  
@rspace_pad:
  mov   byte ptr[edi], ' '
  add   edi,   1
  dec   ebx 
  jne   @rspace_pad 
  
exit:
  mov   eax, edi 
  sub   eax, esi 
  
  stk_pop = $pop (ebx, ebp, esi, edi)
  
  ret    
  
format$x_$o_$b endp

;; --------------------------------------------------------------------
format$f proc near SYMBOL      
    
  ;; Process format %f
  
  ;; buffer:[esp+4]
  ;; flags:[esp+8]
  ;; width:[esp+12]
  ;; precision:[esp+16]
  ;; 
  ;; length_size:[esp+20] 32|64|80
  ;; 
  ;; value-lo[esp+24]
  ;; value-hi[esp+28] (only for unsigned int 64)
  ;; value-hi2[esp+32] (only for unsigned int 64)
  ;;
  ;; isuppercase[esp+36]
  ;; 
  ;; temp use[esp+40]
  ;; temp use2[esp+44]
  
  ;; ret:use buffer length 
  
  ;; 1.#INF00 
  ;;-1.#IND00 
  ;;-1.#INF00
ARG_BUFFER = 4 
ARG_FLAGS = 8 
ARG_WIDTH = 12 
ARG_PRECISION = 16 
ARG_LENGTH = 20
ARG_VALUE_LO = 24 
ARG_VALUE_HI = 28 
ARG_VALUE_HI2 = 32
ARG_ISUPPERCASE = 36
ARG_TEMP_USE = 40
ARG_TEMP_USE2 = 44  ;; 0 normal 1:nan 2:-inf 3:+inf

  ;; clear float context 
  finit 
  
  stk_push = $push (edi, esi, ebp, ebx) 
  
  mov   edi, $stack (stk_push, ARG_BUFFER) 
  mov   ebp, $stack (stk_push, ARG_FLAGS)
  
  mov   ecx, $stack (stk_push, ARG_LENGTH)
  nop 
  
  mov   dword ptr $stack (stk_push, ARG_TEMP_USE), 0
  mov   dword ptr $stack (stk_push, ARG_TEMP_USE2), 0
  
  cmp   ecx, 80 
  jne   @F 
  
  fld   tbyte ptr $stack (stk_push, ARG_VALUE_LO) ;; Load float-80 
  jmp   @L

@@:
  cmp   ecx, 64 
  jne   @F
  
  fld   qword ptr $stack (stk_push, ARG_VALUE_LO) ;; Load float-64 
  jmp   @L

@@:
  fld   dword ptr $stack (stk_push, ARG_VALUE_LO) ;; Load float-32 

@L:
  ;; st(0) float value 
  fld   st(0)
  fst   dword ptr $stack (stk_push, ARG_TEMP_USE)
  fstp  tbyte ptr $stack (stk_push, ARG_VALUE_LO) ;; Store to stack memory 

VAR_F80       equ tbyte ptr $stack (stk_push, ARG_VALUE_LO)
VAR_DECIMAL   equ tbyte ptr $stack (stk_push, ARG_VALUE_LO)
VAR_BLOCK_INT equ dword ptr $stack (stk_push, ARG_LENGTH)
  
  ;;  Fetch float-32
  mov   eax,  $stack (stk_push, ARG_TEMP_USE)
  mov   edx,  eax 
  and   eax,  0ff800000h 
  mov   ebx,  eax 
  mov   esi,  edx
  shl   esi,  9
   
  ;;     Check nan 
  sub   esi,  1 
  sbb   esi,  esi 
  shl   eax,  1      
  add   eax,  001000000h
  or    eax,  esi 
  mov   eax,  0 
  mov   esi,  1
  cmovz eax, esi 
  ;;     Check -inf  
  cmp   edx, 0ff800000h
  mov   esi, 2
  cmovz eax, esi 
  ;;     Check +inf  
  cmp   edx, 07f800000h
  mov   esi, 3
  cmovz eax, esi 
  
  mov  $stack (stk_push, ARG_TEMP_USE2), eax 
  
  test  eax, eax 
  je    @F 
  
  cmp   eax, 3
  je    @inf_post
  
  mov   dword ptr $stack (stk_push, ARG_TEMP_USE), -1
  fild  dword ptr $stack (stk_push, ARG_TEMP_USE)
  fstp  VAR_F80
  jmp   @F 
  
@inf_post:
  fld1  
  fstp  VAR_F80
@@:
  shl   edx, 1
  jnc   @F  

  fld VAR_F80 ;;-----------------------------------
  fchs  ;; remove sign 
  fstp VAR_F80

  or    ebp,      FSM_MASK_NEG          ;; Add sign bit mask                         
  and   ebp, not FCM_MASK_SIGN_BIT     ;; Remove flags::plus 
  and   ebp, not FCM_MASK_SPACE_OR_NEG ;; Remove flags::space 
  
@@:
  test  edx, edx 
  je    @F        ;; Is -0.0 or +0.0 ???
  
  ;; Add round. 
  ;; 80 bit := 10 bytes 
  ;; 
  mov   ebx, $stack (stk_push, ARG_PRECISION)
  cmp   ebx, 0
  
  jle   @F 
  
  mov   eax, FTOA_MAX_PRECISION
  
  cmp   ebx,  FTOA_MAX_PRECISION
  cmova ebx,  eax
  
  lea   eax, [ebx * 4 + ebx] ;; x5 
  
  fld   VAR_F80 
  fld   tbyte ptr[ft_precision_push_lut + eax * 2]
  
  faddp 
  
  fstp  VAR_F80
  
@@:
  ;; Format the integer part (reverse)
  fld     VAR_F80                      ;; Load float-80    @x87:st0 
  fisttp  VAR_BLOCK_INT               ;; To integer       @x87:free
  fild    VAR_BLOCK_INT               ;; Load integer to float-80 @x87:st0
  fld     VAR_F80                     ;; st(0)                    @x87:st0, st1 
  fsub    st(0), st(1)               ;; Sub int block, get decimal part @x87:st0, st1
  fstp    VAR_DECIMAL                 ;;          @x87:st0
  fstp    st(0)                      ;;          @x87:free 
  
  mov     esi,  edi                  ;; Save base buffer ptr
  xor     ebx,  ebx 
  
  cmp     VAR_BLOCK_INT, 0
  jge     @F
  
  mov     ebx,  1                   ;; Int part count 
  jmp     @lpad_chk
@@:
  mov     eax,  VAR_BLOCK_INT       ;; Load eax 
  mov     ecx,  10  
@@:
  test    eax,  eax 
  je      @lpad_chk 
  
  xor     edx,  edx
  idiv    ecx
  
  add     ebx,  1 
  jmp     @B
  
@lpad_chk:
  test    ebp,  FCM_MASK_LEFT_ALIGN 
  jne     @F 
  
  mov     ecx,  ebx 
  lea     edx,  [ebx + 1] 

  test    ebp,  FCM_MASK_SPACE_OR_NEG
  cmovne  ebx,  edx 
  
  test    ebp,  FCM_MASK_SIGN_BIT
  cmovne  ebx,  edx 
  
  test    ebp,  FSM_MASK_NEG
  cmovne  ebx,  edx 
  
  mov     edx,  $stack (stk_push, ARG_PRECISION) ;; Load precision
  lea     eax,  [edx + (1 shl 31)]  ;; With dot 
  
  test    edx,  ((1 shl 31) - 1)
  cmovne  edx,  eax 
  
  test    ebp,  FCM_MASK_OUTPUT_PREFIX ;; Keep dot, at least 
  cmovne  edx,  eax
  
  btr     edx,  31 
  adc     edx,  0    ;; Collect dot and decimal_part number 
  
  lea     eax,  [ebx + edx]  ;; Total width 
  
  mov     edx,  $stack (stk_push, ARG_WIDTH) ;; Load settings width 
  sub     edx,  eax 
  
  jle     @F        ;; Fill normal 
  
  ;;  Fill buffer ' ' or '0'
  test    ebp,  FCM_MASK_ZEROPAD 
  jne     @lzero_padp
  
@lspace_pad:
  mov     byte ptr[edi],  ' '  ;; Append left space ' '
  add     edi,  1
  sub     edx,  1 
  jne     @lspace_pad
@@:
  test    ebp, (FSM_MASK_NEG or FCM_MASK_SIGN_BIT or FCM_MASK_SPACE_OR_NEG)
  je      @itoa 
  
  mov     ebx,  ' '
  mov     eax,  '+'
  mov     ecx,  '-'
  
  test    ebp, FCM_MASK_SIGN_BIT ;; High priority
  cmovne  ebx,  eax
  
  test    ebp, FSM_MASK_NEG ;; Highest priority
  cmovne ebx,  ecx

  mov     byte ptr[edi],  bl     ;; Write sign 
  add     edi, 1
  
  jmp     @itoa
  
@lzero_padp:

  ;;      zero   pad 
  test    ebp, (FSM_MASK_NEG or FCM_MASK_SIGN_BIT or FCM_MASK_SPACE_OR_NEG)
  je      @F 
 
  mov   ebx,  ' '
  mov   eax,  '+'
  mov   ecx,  '-'
  
  test  ebp, FCM_MASK_SIGN_BIT ;; High priority
  cmovne ebx,  eax
  
  test  ebp, FSM_MASK_NEG ;; Highest priority
  cmovne ebx,  ecx

  mov   byte ptr[edi],  bl     ;; Write sign 
  add   edi, 1
  
  ;;  Write '0' 
@lzero_pad:
  mov   byte ptr[edi],  '0'
  add   edi,  1
  sub   edx,  1 
  jne   @lzero_pad
  
  ;;  Stuff itoa 
@itoa:
  mov     eax,  VAR_BLOCK_INT       ;; Load eax
  mov     ecx,  10 
  
  mov     ebx,  edi   ;;Save current base ptr 
  
  test    eax,  eax 
  jne     @F 
  
  mov     byte ptr[edi],  '0'
  add     edi,  1
  
  jmp     @out_dot_decimal
  
@@:
  xor     edx,  edx 
  div     ecx 
  
  add     edx,  '0'
  mov     byte ptr[edi], dl 
  
  add     edi,   1
  
  test    eax,   eax
  jne     @B 

  ;; Reverse buffer ch 
  mov     eax, edi 
  sub     eax, ebx ;; count 

  mov     ecx, eax 
  shr     ecx, 1 
  
  test    ecx, ecx 
  je      @out_dot_decimal
  
  lea     edx, [ebx + eax - 1]

@reverse_ch:
  mov     al,  [edx]
  mov     ah,  [ebx]
           
  mov     [ebx], al 
  mov     [edx], ah 
           
  add     ebx, 1 
  sub     edx, 1 
           
  sub     ecx, 1
  jne     @reverse_ch
  
@out_dot_decimal:
  ;; Ouput float and decimal part 
  mov     edx,  $stack (stk_push, ARG_PRECISION) ;; Load precision
  lea     eax,  [edx + (1 shl 31)]  ;; With dot 
  
  test    edx,  ((1 shl 31) - 1)
  cmovne  edx,  eax 
  
  test    ebp,  FCM_MASK_OUTPUT_PREFIX ;; Keep dot, at least 
  cmovne  edx,  eax
  
  test    edx,  (1 shl 31)
  je      @F 
   
  mov     byte ptr[edi],  '.'       ;; Output dot.
  add     edi,  1
  
  and     edx, not (1 shl 31)      ;; Clear dot mask 
  je      @F
  
  ;;      exist  decimal part 
  
  xor     ebx, ebx    ;; Count 
  
  mov     eax,  $stack (stk_push, ARG_TEMP_USE2)
  test    eax,  eax 
  jne     @out_infd 
  
  finit  ;; Clear x87fpu context 
  
x87fpu_cast:
  
  fld     ft_10       ;; Load 10.0       @x87:st(0)
  fld     VAR_DECIMAL ;; Load decimal    @x87：st(0), st(1)
  fmul    st(0), st(1) ;; Result it    
  fstp    VAR_DECIMAL ;; Store it       @x87:st(0)
  fstp    st(0)       ;; @x87:free 

  fld     VAR_DECIMAL        
  fisttp   VAR_BLOCK_INT      ;; Cast to int @x87:free 
  
  fild    VAR_BLOCK_INT 
  fld     VAR_DECIMAL      ;; st(0)
  fsub    st(0), st(1) ;; Sub int block
  fstp    VAR_DECIMAL
  fstp    st(0)       ;; @x87:free 
  
  mov     eax, VAR_BLOCK_INT
  lea     eax, [eax + '0']
  
  cmp     ebx, FTOA_MAX_PRECISION
  mov     ecx, '0' 
  cmovae  eax, ecx 
  
  mov     byte ptr[edi],  al       ;; Output ascii 
  add     edi,  1
  
  add     ebx,  1
  
  sub     edx,  1 
  jne     x87fpu_cast
  
  jmp     @F 
  
@out_infd:

  .const 
   @ind db "#ind", 0
   @ind2 db "#IND", 0 
   @inf db "#inf", 0
   @inf2 db "#INF", 0  
   @infd_lut dd offset @ind, offset @ind2, offset  @inf, offset @inf2
  .code 
  
  mov     eax,  $stack (stk_push, ARG_ISUPPERCASE)
  test    eax,  eax 
  mov     eax,  0 
  mov     ecx,  4 
  cmovne  eax,  ecx 
  
  mov     ecx,  $stack (stk_push, ARG_TEMP_USE2) 
  shr     ecx,  1
  
  mov     eax,  [ecx * 8 + eax + @infd_lut]
 
out_infd:
  
  mov     cl,   byte ptr[eax]
  test    cl,   cl 
  je      @F 
  
  mov     byte ptr[edi],  cl       ;; Output ascii 
  add     edi,  1
  
  add     eax,  1
  
  sub     edx,  1 
  jne     out_infd
  
@@:
  ;; Check precision pad 
  test    edx,  edx 
  je      @F 

@precpad:
  mov     byte ptr[edi], '0' ;;  Fill right space 
  add     edi,   1
  dec     edx 
  jne     @precpad  
 
@@:
  test    ebp,  FCM_MASK_LEFT_ALIGN 
  je      @exit
  
  ;;      Fill right space 
  mov     eax,  edi 
  sub     eax,  esi   ;; Get render count 

  mov     edx,  $stack (stk_push, ARG_WIDTH) ;; Load settings width 
  sub     edx,  eax 
  
  jle     @exit 

@@:
  mov     byte ptr[edi], ' ' ;;  Fill right space 
  add     edi,   1
  dec     edx 
  jne     @B 
  
@exit:
  mov     eax, edi 
  sub     eax, esi 
    
  stk_pop = $pop (edi, esi, ebp, ebx) 
  
  ret      
  
format$f endp

;; --------------------------------------------------------------------
format$e proc near SYMBOL      
    
  ;; Process format %f
  
  ;; buffer:[esp+4]
  ;; flags:[esp+8]
  ;; width:[esp+12]
  ;; precision:[esp+16]
  ;; 
  ;; length_size:[esp+20] 32|64|80
  ;; 
  ;; value-lo[esp+24]
  ;; value-hi[esp+28] (only for unsigned int 64)
  ;; value-hi2[esp+32] (only for unsigned int 64)
  ;;
  ;; isuppercase[esp+36]
  ;; 
  ;; temp use[esp+40]
  ;; temp use2[esp+44]
  ;; temp use3[esp+48]
  
  ;; ret:use buffer length 
  
  ;;output sign int . decimal e + or - nnn (dec mode )

ARG_BUFFER = 4 
ARG_FLAGS = 8 
ARG_WIDTH = 12 
ARG_PRECISION = 16 
ARG_LENGTH = 20
ARG_VALUE_LO = 24 
ARG_VALUE_HI = 28 
ARG_VALUE_HI2 = 32
ARG_ISUPPERCASE = 36
ARG_TEMP_USE = 40
ARG_TEMP_USE2 = 44  ;; 0 normal 1:nan 2:-inf 3:+inf
ARG_TEMP_USE3 = 48  ;; E shift 

  ;; clear float context 
  finit 
  
  stk_push = $push (edi, esi, ebp, ebx) 
  
  mov   edi, $stack (stk_push, ARG_BUFFER) 
  mov   ebp, $stack (stk_push, ARG_FLAGS)
  
  mov   ecx, $stack (stk_push, ARG_LENGTH)
  nop 
  
  mov   dword ptr $stack (stk_push, ARG_TEMP_USE), 0
  mov   dword ptr $stack (stk_push, ARG_TEMP_USE2), 0
  mov   dword ptr $stack (stk_push, ARG_TEMP_USE3), 0  
  
  cmp   ecx, 80 
  jne   @F 
  
  fld   tbyte ptr $stack (stk_push, ARG_VALUE_LO) ;; Load float-80 
  jmp   @L

@@:
  cmp   ecx, 64 
  jne   @F
  
  fld   qword ptr $stack (stk_push, ARG_VALUE_LO) ;; Load float-64 
  jmp   @L

@@:
  fld   dword ptr $stack (stk_push, ARG_VALUE_LO) ;; Load float-32 

@L:
  ;; st(0) float value 
  fld   st(0)
  fst   dword ptr $stack (stk_push, ARG_TEMP_USE)
  fstp  tbyte ptr $stack (stk_push, ARG_VALUE_LO) ;; Store to stack memory 
  
VAR_F80       equ tbyte ptr $stack (stk_push, ARG_VALUE_LO)
VAR_DECIMAL   equ tbyte ptr $stack (stk_push, ARG_VALUE_LO)
VAR_BLOCK_INT equ dword ptr $stack (stk_push, ARG_LENGTH)
  
  ;;  Fetch float-32
  mov   eax,  $stack (stk_push, ARG_TEMP_USE)
  mov   edx,  eax 
  and   eax,  0ff800000h 
  mov   ebx,  eax 
  mov   esi,  edx
  shl   esi,  9
   
  ;;     Check nan 
  sub   esi,  1 
  sbb   esi,  esi 
  shl   eax,  1      
  add   eax,  001000000h
  or    eax,  esi 
  mov   eax,  0 
  mov   esi,  1
  cmovz eax, esi 
  ;;     Check -inf  
  cmp   edx, 0ff800000h
  mov   esi, 2
  cmovz eax, esi 
  ;;     Check +inf  
  cmp   edx, 07f800000h
  mov   esi, 3
  cmovz eax, esi 
  
  mov  $stack (stk_push, ARG_TEMP_USE2), eax 
  
  test  eax, eax 
  je    @F 
  
  cmp   eax, 3
  je    @inf_post
  
  mov   dword ptr $stack (stk_push, ARG_TEMP_USE), -1
  fild  dword ptr $stack (stk_push, ARG_TEMP_USE)
  fstp  VAR_F80
  jmp   @F 
  
@inf_post:
  fld1  
  fstp  VAR_F80
@@:
  
  shl   edx, 1
  jnc   @F  

  fld VAR_F80 ;;-----------------------------------
  fchs  ;; remove sign 
  fstp VAR_F80

  or    ebp,      FSM_MASK_NEG          ;; Add sign bit mask                         
  and   ebp, not FCM_MASK_SIGN_BIT     ;; Remove flags::plus 
  and   ebp, not FCM_MASK_SPACE_OR_NEG ;; Remove flags::space 
  
@@:
  test  edx, edx 
  je    @F        ;; Is -0.0 or +0.0 ???

  ;; Add round.  @TODO:Or after???...
  ;; 80 bit := 10 bytes 
  ;; 
  mov   ebx, $stack (stk_push, ARG_PRECISION)
  cmp   ebx, 0
  
  jle   @F 
  
  mov   eax, FTOA_MAX_PRECISION
  
  cmp   ebx,  FTOA_MAX_PRECISION
  cmova ebx,  eax
  
  lea   eax, [ebx * 4 + ebx] ;; x5 
  
  fld   VAR_F80 
  fld   tbyte ptr[ft_precision_push_lut + eax * 2]
  
  faddp 
  
  fstp  VAR_F80
  
  ;; Convert base 0~9 and E shift 
  fld     VAR_F80                      ;; Load float-80    @x87:st0 
  fisttp  VAR_BLOCK_INT               ;; To integer       @x87:free
  
  mov     eax,  VAR_BLOCK_INT     ;; Load int block
                                  ;; Is 1 ~9 ???
  lea     ecx,  [eax - 1]
  cmp     ecx,  8 
  jb      @F 
  
  test    eax,  eax 
  je      @neg_e_sft 
  
  ;;       Postive E shift 
  xor     ebx,  ebx
  mov     ecx,  10 
  
@pe_sftl:
  xor     edx,  edx 
  
  idiv    ecx 
  
  test    eax,  eax 
  je      @pe_sftbrp
  
  add     ebx,  1 
  jmp     @pe_sftl
  
@pe_sftbrp:
  mov     $stack (stk_push, ARG_TEMP_USE3),  ebx 
  ;;      Add base 10^.
  mov     edx,  10 
    
@pe_sftbr:
  imul    edx,  edx,  10
  
  sub     ebx,  1 
  jne     @pe_sftbr
  
  mov     VAR_BLOCK_INT,  edx 
  
  fld     VAR_F80          ;; 
  fild    VAR_BLOCK_INT    ;; 10^.
  fdivp
  fstp    VAR_F80
  
  jmp     @F 
  
@neg_e_sft:
  ;;       Negative E shift 
  xor     ebx,  ebx

@ne_sftbr:
  fld     VAR_F80          ;; 
  fld     ft_10
  fmulp  
  fstp    VAR_F80
  fld     VAR_F80          ;;
  fisttp  VAR_BLOCK_INT

  mov     eax,  VAR_BLOCK_INT
  sub     ebx,  1
  test    eax,  eax 
  je      @ne_sftbr
  
  mov     $stack (stk_push, ARG_TEMP_USE3),  ebx

@@:
  ;; Format the integer part (reverse)
  fld     VAR_F80                      ;; Load float-80    @x87:st0 
  fisttp  VAR_BLOCK_INT               ;; To integer       @x87:free
  fild    VAR_BLOCK_INT               ;; Load integer to float-80 @x87:st0
  fld     VAR_F80                     ;; st(0)                    @x87:st0, st1 
  fsub    st(0), st(1)               ;; Sub int block, get decimal part @x87:st0, st1
  fstp    VAR_DECIMAL                 ;;          @x87:st0
  fstp    st(0)                      ;;          @x87:free 
  
  mov     esi,  edi                  ;; Save base buffer ptr
  xor     ebx,  ebx 
  
  cmp     VAR_BLOCK_INT, 0
  jge     @F
  
  mov     ebx,  1                   ;; Int part count 
  jmp     @lpad_chk
@@:
  mov     eax,  VAR_BLOCK_INT       ;; Load eax 
  mov     ecx,  10  
@@:
  test    eax,  eax 
  je      @lpad_chk 
  
  xor     edx,  edx
  idiv    ecx
  
  add     ebx,  1 
  jmp     @B
  
@lpad_chk:
  test    ebp,  FCM_MASK_LEFT_ALIGN 
  jne     @F 
  
  mov     ecx,  ebx 
  lea     edx,  [ebx + 1] 

  test    ebp,  FCM_MASK_SPACE_OR_NEG
  cmovne  ebx,  edx 
  
  test    ebp,  FCM_MASK_SIGN_BIT
  cmovne  ebx,  edx 
  
  test    ebp,  FSM_MASK_NEG
  cmovne  ebx,  edx 
  
  mov     edx,  $stack (stk_push, ARG_PRECISION) ;; Load precision
  lea     eax,  [edx + (1 shl 31)]  ;; With dot 
  
  test    edx,  ((1 shl 31) - 1)
  cmovne  edx,  eax 
  
  test    ebp,  FCM_MASK_OUTPUT_PREFIX ;; Keep dot, at least 
  cmovne  edx,  eax
  
  btr     edx,  31 
  adc     edx,  0    ;; Collect dot and decimal_part number 
  
  lea     eax,  [ebx + edx]  ;; Total width 
  
  mov     edx,  $stack (stk_push, ARG_WIDTH) ;; Load settings width 
  sub     edx,  eax 
  
  jle     @F        ;; Fill normal 
  
  ;;  Fill buffer ' ' or '0'
  test    ebp,  FCM_MASK_ZEROPAD 
  jne     @lzero_padp
  
@lspace_pad:
  mov     byte ptr[edi],  ' '  ;; Append left space ' '
  add     edi,  1
  sub     edx,  1 
  jne     @lspace_pad
@@:
  test    ebp, (FSM_MASK_NEG or FCM_MASK_SIGN_BIT or FCM_MASK_SPACE_OR_NEG)
  je      @itoa 
  
  mov     ebx,  ' '
  mov     eax,  '+'
  mov     ecx,  '-'
  
  test    ebp, FCM_MASK_SIGN_BIT ;; High priority
  cmovne  ebx,  eax
  
  test    ebp, FSM_MASK_NEG ;; Highest priority
  cmovne ebx,  ecx

  mov     byte ptr[edi],  bl     ;; Write sign 
  add     edi, 1
  
  jmp     @itoa
  
@lzero_padp:

  ;;      zero   pad 
  test    ebp, (FSM_MASK_NEG or FCM_MASK_SIGN_BIT or FCM_MASK_SPACE_OR_NEG)
  je      @F 
 
  mov   ebx,  ' '
  mov   eax,  '+'
  mov   ecx,  '-'
  
  test  ebp, FCM_MASK_SIGN_BIT ;; High priority
  cmovne ebx,  eax
  
  test  ebp, FSM_MASK_NEG ;; Highest priority
  cmovne ebx,  ecx

  mov   byte ptr[edi],  bl     ;; Write sign 
  add   edi, 1
  
  ;;  Write '0' 
@lzero_pad:
  mov   byte ptr[edi],  '0'
  add   edi,  1
  sub   edx,  1 
  jne   @lzero_pad
  
  ;;  Stuff itoa 
@itoa:
  mov     eax,  VAR_BLOCK_INT       ;; Load eax
  mov     ecx,  10 
  
  mov     ebx,  edi   ;;Save current base ptr 
  
  test    eax,  eax 
  jne     @F 
  
  mov     byte ptr[edi],  '0'
  add     edi,  1
  
  jmp     @out_dot_decimal
  
@@:
  xor     edx,  edx 
  div     ecx 
  
  add     edx,  '0'
  mov     byte ptr[edi], dl 
  
  add     edi,   1
  
  test    eax,   eax
  jne     @B 

  ;; Reverse buffer ch 
  mov     eax, edi 
  sub     eax, ebx ;; count 

  mov     ecx, eax 
  shr     ecx, 1 
  
  test    ecx, ecx 
  je      @out_dot_decimal
  
  lea     edx, [ebx + eax - 1]

@reverse_ch:
  mov     al,  [edx]
  mov     ah,  [ebx]
           
  mov     [ebx], al 
  mov     [edx], ah 
           
  add     ebx, 1 
  sub     edx, 1 
           
  sub     ecx, 1
  jne     @reverse_ch
  
@out_dot_decimal:
  ;; Ouput float and decimal part 
  mov     edx,  $stack (stk_push, ARG_PRECISION) ;; Load precision
  lea     eax,  [edx + (1 shl 31)]  ;; With dot 
  
  test    edx,  ((1 shl 31) - 1)
  cmovne  edx,  eax 
  
  test    ebp,  FCM_MASK_OUTPUT_PREFIX ;; Keep dot, at least 
  cmovne  edx,  eax
  
  test    edx,  (1 shl 31)
  je      @F 
   
  mov     byte ptr[edi],  '.'       ;; Output dot.
  add     edi,  1
  
  and     edx, not (1 shl 31)      ;; Clear dot mask 
  je      @F
  
  ;;      exist  decimal part 
  
  xor     ebx, ebx    ;; Count 
  
  mov     eax,  $stack (stk_push, ARG_TEMP_USE2)
  test    eax,  eax 
  jne     @out_infd 
  
  finit  ;; Clear x87fpu context 
  
x87fpu_cast:
  
  fld     ft_10       ;; Load 10.0       @x87:st(0)
  fld     VAR_DECIMAL ;; Load decimal    @x87：st(0), st(1)
  fmul    st(0), st(1) ;; Result it    
  fstp    VAR_DECIMAL ;; Store it       @x87:st(0)
  fstp    st(0)       ;; @x87:free 

  fld     VAR_DECIMAL        
  fisttp   VAR_BLOCK_INT      ;; Cast to int @x87:free 
  
  fild    VAR_BLOCK_INT 
  fld     VAR_DECIMAL      ;; st(0)
  fsub    st(0), st(1) ;; Sub int block
  fstp    VAR_DECIMAL
  fstp    st(0)       ;; @x87:free 
  
  mov     eax, VAR_BLOCK_INT
  lea     eax, [eax + '0']
  
  cmp     ebx, FTOA_MAX_PRECISION
  mov     ecx, '0' 
  cmovae  eax, ecx 
  
  mov     byte ptr[edi],  al       ;; Output ascii 
  add     edi,  1
  
  add     ebx,  1
  
  sub     edx,  1 
  jne     x87fpu_cast
  
  jmp     @F 
  
@out_infd:

  .const 
  ;; @ind db "#ind", 0
  ;; @ind2 db "#IND", 0 
  ;; @inf db "#inf", 0
  ;; @inf2 db "#INF", 0  
  ;; @infd_lut dd offset @ind, offset @ind2, offset  @inf, offset @inf2
  .code 
  
  mov     eax,  $stack (stk_push, ARG_ISUPPERCASE)
  test    eax,  eax 
  mov     eax,  0 
  mov     ecx,  4 
  cmovne  eax,  ecx 
  
  mov     ecx,  $stack (stk_push, ARG_TEMP_USE2) 
  shr     ecx,  1
  
  mov     eax,  [ecx * 8 + eax + @infd_lut]
 
out_infd:
  
  mov     cl,   byte ptr[eax]
  test    cl,   cl 
  je      @F 
  
  mov     byte ptr[edi],  cl       ;; Output ascii 
  add     edi,  1
  
  add     eax,  1
  
  sub     edx,  1 
  jne     out_infd
  
@@:
  ;; Check precision pad 
  test    edx,  edx 
  je      @epadp

@precpad:
  mov     byte ptr[edi], '0' ;;  Fill right space 
  add     edi,   1
  dec     edx 
  jne     @precpad  
 
@epadp:
  mov     eax,  $stack (stk_push, ARG_ISUPPERCASE)
  test    eax,  eax 
  mov     eax,  'e' 
  mov     ecx,  'E' 
  cmovne  eax,  ecx 
  
  mov     [edi], al 
  add     edi,   1 
  
  mov     eax,  $stack (stk_push, ARG_TEMP_USE3) ;; E shift value 
  mov     edx,  '+' 
  mov     ecx,  '-' 
  test    eax,  080000000h 
  cmovne  edx,  ecx 
  
  mov     [edi], dl ;;  sign
  add     edi,   1 
  
  cdq     
  xor     eax,  edx 
  sub     eax,  edx      ;; Value 

  xor     edx,  edx
  mov     ecx,  100   
  idiv    ecx 
  
  add     eax,  '0'
  
  mov     [edi], al ;;  n00 
  add     edi,   1  
  
  mov     eax,  edx
  xor     edx,  edx
  mov     ecx,  10   
  idiv    ecx 
  
  mov     ah,   dl 
  add     eax,  '0' or ('0' shl 8)
  
  mov     [edi], ax ;;  0nn 
  add     edi,   2  
  
@rpadp:
  test    ebp,  FCM_MASK_LEFT_ALIGN 
  je      @exit
  
  ;;      Fill right space 
  mov     eax,  edi 
  sub     eax,  esi   ;; Get render count 

  mov     edx,  $stack (stk_push, ARG_WIDTH) ;; Load settings width 
  sub     edx,  eax 
  
  jle     @exit 

@@:
  mov     byte ptr[edi], ' ' ;;  Fill right space 
  add     edi,   1
  dec     edx 
  jne     @B 
  
@exit:
  mov     eax, edi 
  sub     eax, esi 
    
  stk_pop = $pop (edi, esi, ebp, ebx) 
  
  ret      
  
format$e endp

;; --------------------------------------------------------------------
format$c proc near SYMBOL   
    
  ;; Process format %c
  
  ;; buffer:[esp+4]
  ;; flags:[esp+8]
  ;; width:[esp+12]
  ;; precision:[esp+16]
  ;; 
  ;; length_size:[esp+20] 8:const char * | 16:const wchar_t *
  ;; 
  ;; dbcs/ucs2 character [esp+24]

  ;; ret:use buffer length 
ARG_BUFFER = 4 
ARG_FLAGS = 8 
ARG_WIDTH = 12 
ARG_PRECISION = 16 
ARG_LENGTH = 20
ARG_CH = 24 

  stk_push = $push (ebx, ebp, esi, edi)
  
  mov   edi, $stack (stk_push, ARG_BUFFER)  
  mov   ebp, $stack (stk_push, ARG_FLAGS) 
         
  mov   ecx, $stack (stk_push, ARG_LENGTH) 
  nop        
  
  mov   esi, edi 
  
  cmp   ecx, 8 
  je    @F 
  
  lea   edx,  $stack (stk_push, ARG_CH)   
  mov   dword ptr[edx], 0
  
  $ccall  wcstombs, edx, eax, ((1 shl 31) - 1) 
@@:
  mov   eax, $stack (stk_push, ARG_CH)
  xor   ecx,  ecx 
  
  mov   edx,  1 
  test  eax,  eax 
  
  cmovne ecx, edx 
  
  xor   edx,  edx 
  
  test  eax,  128 
;;cmovne ecx, edx

  mov   edx, $stack (stk_push, ARG_PRECISION) 
  
  mov   ebx, ((1 shl 31) - 1)
  cmp   edx, 0 
  cmovl edx, ebx 
  
  cmp   ecx,  edx  ;;string length ? precision
  cmovge ecx, edx
  
  ;; Check left pad ???
  test  ebp,  FCM_MASK_LEFT_ALIGN
  jne   @dbcscpyp
  
  ;; Check width 
  mov   edx, $stack (stk_push, ARG_WIDTH) ;; Load width 
  sub   edx, ecx               ;; Width - string length 

  jle   @dbcscpyp 
  
  ;; Output ' ' or '0'
  push  ecx   ;; XXX: Save it.
  
  mov   ebx, ' '
  mov   ecx, '0'
  test  ebp, FCM_MASK_ZEROPAD 
  cmovne ebx, ecx 
  
  pop   ecx   
  
@lpad:
  mov   byte ptr[edi],  bl
  add   edi,  1
  sub   edx,  1 
  jne   @lpad

@dbcscpyp:
  test  ecx,  ecx 
  je    @rpadp 
  
  lea   eax, $stack (stk_push, ARG_CH)   
  movzx ebx, byte ptr[eax] 
  nop      
  
  mov   [edi], bl ;; Store dbcs first byte  
  shr   ebx, 7
  
  mov   dl, byte ptr[eax + ebx] ;; Load dbcs dummy/secondary byte 
  mov   byte ptr[edi + ebx], dl ;; Store dbcs dummy/secondary byte 
  
  lea   edi, [edi + ebx + 1] 
  
@rpadp:
  
  ;; Check width 
  test  ebp,  FCM_MASK_LEFT_ALIGN
  je    @exit
  
  mov   edx, $stack (stk_push, ARG_WIDTH);; Load width 
  sub   edx, ecx               ;; Width - string length 
  
  jle   @exit 
  
@rpad:
  mov   byte ptr[edi],  ' '
  add   edi,  1
  sub   edx,  1 
  jne   @rpad
  
@exit:
  mov   eax, edi 
  sub   eax, esi 
  
  stk_pop = $pop (ebx, ebp, esi, edi)
  
  ret    
  
format$c endp

;; --------------------------------------------------------------------
format$s proc near SYMBOL   
    
  ;; @BUG: A single dbcs character is the opposite of the sequence in a string????
  ;; 
  ;; Process format %s
  
  ;; buffer:[esp+4]
  ;; flags:[esp+8]
  ;; width:[esp+12]
  ;; precision:[esp+16]
  ;; 
  ;; length_size:[esp+20] 8:const char * | 16:const wchar_t *
  ;; 
  ;; dbcs/ucs2 buffer pointer [esp+24]
  ;; Indicates whether it is ucs2 buffer [esp+28] bool
  
  ;; ret:use buffer length 
ARG_BUFFER = 4 
ARG_FLAGS = 8 
ARG_WIDTH = 12 
ARG_PRECISION = 16 
ARG_LENGTH = 20
TMP_CHARCOUNT = 20
ARG_CBUFFER = 24 
ARG_ISUCS2BUFFER = 28 

  stk_push = $push (ebx, ebp, esi, edi)
  
  mov   edi, $stack (stk_push, ARG_BUFFER)  
  mov   ebp, $stack (stk_push, ARG_FLAGS) 
         
  mov   ecx, $stack (stk_push, ARG_LENGTH) 
  nop        
  
  mov   eax, $stack (stk_push, ARG_CBUFFER)     
  mov   edx, $stack (stk_push, ARG_ISUCS2BUFFER)
  
  mov   esi, edi 
  
  test  eax, eax 
  je    @null_out
  
  test  edx, edx 
  jne   @ucs2_stuff 
  
  cmp   ecx, 16 
  je    @ucs2_stuff 
  
  jmp   @F 
  
@null_out:
  ;; Stuff "(null)"
  
  ;; XXX:Make temp string buffer in stack args.
  lea eax, $stack (stk_push, ARG_CBUFFER) 

  mov byte ptr[eax+0], '('
  mov byte ptr[eax+1], 'n' 
  mov byte ptr[eax+2], 'u'
  mov byte ptr[eax+3], 'l' 
  mov byte ptr[eax+4], 'l'
  mov byte ptr[eax+5], ')'
  mov byte ptr[eax+6], 0
  
@@:
;;  Dbcs stuff 
;;  
;;  Calculates the actual length of the dbcs character
  xor   ecx, ecx 
  mov   edx, eax  
  
  xor   ebx, ebx 

@@:
  mov   bl, byte ptr[edx] 
  nop      
  
  test  ebx, ebx 
  je    @F 
  
  shr   ebx, 7
  nop  
  
  lea   edx, [edx + ebx + 1]
  nop 
  
  add   ecx, 1 
  
  jmp   @B 
  
@@:
  mov   edx, $stack (stk_push, ARG_PRECISION) 
  
  mov   ebx, ((1 shl 31) - 1)
  cmp   edx, 0 
  cmovl edx, ebx 
  
  cmp   ecx,  edx  ;;string length ? precision
  cmovge ecx, edx
  
  ;; Check left pad ???
  test  ebp,  FCM_MASK_LEFT_ALIGN
  jne   @dbcscpyp
  
  ;; Check width 
  mov   edx, $stack (stk_push, ARG_WIDTH) ;; Load width 
  sub   edx, ecx               ;; Width - string length 

  jle   @dbcscpyp 
  
  ;; Output ' ' or '0'
  push  ecx   ;; XXX: Save it.
  
  mov   ebx, ' '
  mov   ecx, '0'
  test  ebp, FCM_MASK_ZEROPAD 
  cmovne ebx, ecx 
  
  pop   ecx   
  
@lpad:
  mov   byte ptr[edi],  bl
  add   edi,  1
  sub   edx,  1 
  jne   @lpad

@dbcscpyp:
  xor   ebx,  ebx
  push  ecx 
  
  test  ecx,  ecx 
  je    @rpadp 
  
@dbcscpy:
  mov   bl, byte ptr[eax] 
  nop      
  
  mov   [edi], bl ;; Store dbcs first byte  
  shr   ebx, 7
  
  mov   dl, byte ptr[eax + ebx] ;; Load dbcs dummy/secondary byte 
  mov   byte ptr[edi + ebx], dl ;; Store dbcs dummy/secondary byte 
  
  lea   eax, [eax + ebx + 1]
  lea   edi, [edi + ebx + 1] 
  
  sub   ecx, 1 
  jne   @dbcscpy
  
@rpadp:
  
  pop   ecx
  
  ;; Check width 
  test  ebp,  FCM_MASK_LEFT_ALIGN
  je    @exit
  
  mov   edx, $stack (stk_push, ARG_WIDTH);; Load width 
  sub   edx, ecx               ;; Width - string length 
  
  jle   @exit 
  
@rpad:
  mov   byte ptr[edi],  ' '
  add   edi,  1
  sub   edx,  1 
  jne   @rpad
  
@exit:
  mov   eax, edi 
  sub   eax, esi 
  
  stk_pop = $pop (ebx, ebp, esi, edi)
  
  ret    

@ucs2_stuff:
  ;; wcstombs 
  mov   byte ptr[edi], 0
  
  push  eax 
  
  $ccall  wcstombs, edi, eax, ((1 shl 31) - 1)
  
  test  eax, eax 
  je    @F 
  
  mov   ecx, eax 
  
  pop   eax ;;  Resume  
  
  mov   eax, edi 
  mov   $stack (stk_push, TMP_CHARCOUNT), ecx 
@@:
;;  Dbcs stuff 
;;  
;;  Calculates the actual length of the dbcs character
  xor   ecx, ecx 
  mov   edx, edi  
  
  xor   ebx, ebx 

@@:
  mov   bl, byte ptr[edx] 
  nop      
  
  test  ebx, ebx 
  je    @F 
  
  shr   ebx, 7
  nop  
  
  lea   edx, [edx + ebx + 1]
  nop 
  
  add   ecx, 1 
  
  jmp   @B 
  
@@:
  mov   edx, $stack (stk_push, ARG_PRECISION);; precision 
  
  mov   ebx, ((1 shl 31) - 1)
  cmp   edx, 0 
  cmovl edx, ebx 
  
  cmp   ecx,  edx  ;;string length ? precision
  cmovge ecx, edx
  
  ;; Check left pad ???
  test  ebp,  FCM_MASK_LEFT_ALIGN
  jne   @dbcscpyp2
  
  ;; Check width 
  mov   edx, $stack (stk_push, ARG_WIDTH);; Load width 
  sub   edx, ecx               ;; Width - string length 

  jle   @dbcscpyp2
  
  stk_push2 = stk_push + $push (ecx, esi)
  
  ;; Move string buffer 
  mov   esi, $stack (stk_push2, TMP_CHARCOUNT) ;; buffer count 
  lea   ebx, [edi+esi-1]

@@:
  mov   al,  byte ptr[ebx]
  mov   byte ptr[ebx+edx], al 
  
  sub   ebx, 1 
  sub   esi, 1 
  jne   @B 

  ;; Output ' ' or '0'
  mov   ebx, ' '
  mov   ecx, '0'
  test  ebp, FCM_MASK_ZEROPAD 
  cmovne ebx, ecx 
  
  lea   eax,  [edi+edx]
  
  stk_pop2 = $pop  (ecx, esi)
  
@lpad2:
  mov   byte ptr[edi],  bl
  add   edi,  1
  sub   edx,  1 
  jne   @lpad2

@dbcscpyp2:
  xor   ebx,  ebx 
  mov   edx,  ecx 
  
  test  ecx,  ecx 
  je    @rpadp2
  
@dbcscpy2:
  mov   bl, byte ptr[eax] 
  nop      
   
  shr   ebx, 7
  
  lea   eax, [eax + ebx + 1]
  lea   edi, [edi + ebx + 1] 
  
  sub   ecx, 1 
  jne   @dbcscpy2

@rpadp2:
  
  ;; Check width 
  test  ebp,  FCM_MASK_LEFT_ALIGN
  je    @exit2

  mov   eax, $stack (stk_push, ARG_WIDTH);; Load width 
  sub   eax, edx               ;; Width - string length 
  
  jle   @exit2
  
@rpad2:
  mov   byte ptr[edi],  ' '
  add   edi,  1
  sub   eax,  1 
  jne   @rpad2
  
@exit2:
  mov   eax, edi 
  sub   eax, esi 
  
  stk_pop = $pop (ebx, ebp, esi, edi)
  
  ret   

format$s endp

;; int __cdecl _rtl_vsprintf (char *buffer, const char *format, va_list arg)
  
_rtl_vsprintf proc public 
  
ARG_BUFFER = 4 
ARG_FORMAT = 8 
ARG_VA_ARGS = 12 
 
VAR_FLAGS = -16 
VAR_WIDTH = -20 
VAR_PRECISION = -24 
VAR_LENGTH = -28
VAR_FORMAT_STEP_BASE = -32

STACK_VAR_SIZE = 256 
  
  stk_push = $push (ebx, ebp, esi, edi)
  stk_push = stk_push + STACK_VAR_SIZE
  
  sub   esp,  STACK_VAR_SIZE
  
  mov   edi,  $stack (stk_push, ARG_BUFFER)
  mov   esi,  $stack (stk_push, ARG_FORMAT) 
  mov   ebx,  $stack (stk_push, ARG_VA_ARGS)  
  
@formatloop:
 ;; mov     $stack (stk_push, VAR_FORMAT_STEP_BASE), edi 
  
  movzx   eax,   byte ptr[esi] ;; 
  add     esi,   1               ;; ch = *format++;
  
  test    eax,  eax 
  je      @exit 
  
  cmp     eax,  '%'
  je      @F 
  
  mov     byte ptr[edi], al     ;; 
  add     edi,   1               ;; *ouput++ = ch;
  
  jmp     @formatloop

@exit:
  mov     byte ptr[edi], 0
@exit_:
  mov     esi,  $stack (stk_push, ARG_BUFFER)
  sub     edi,  esi 
  mov     eax,  edi 
  
  add     esp,  STACK_VAR_SIZE
  
  stk_pop = $pop (ebx, ebp, esi, edi)
  ret 
  
@@: 
  ;; %[flags][width][.precision][length]type
  
  mov  dword ptr $stack (stk_push, VAR_FLAGS), 0  ;; Clear flags 
  mov  dword ptr $stack (stk_push, VAR_WIDTH), 0  ;; Clear width 
  mov  dword ptr $stack (stk_push, VAR_PRECISION), -1  ;; Clear precsion 
  mov  dword ptr $stack (stk_push, VAR_LENGTH), 0  ;; Clear length 
  
  ;; -----------------------------------------------------------------------
  ;; Collect flags. 
  ;; -----------------------------------------------------------------------
  ;; ' ':Output ' ' or '-' 
  ;; '+':Output '+' or '-'
  ;; '#':Output prefix or other misc 
  ;; '-':Left align, right pad 
  ;; '0':zero pad otherwise space pad 
  ;; -----------------------------------------------------------------------
@flags:
  movzx   eax,   byte ptr[esi] ;; Fetch character
  xor     ecx,   ecx 
  
  test    eax,   eax  ;; Is null??
  je      @exit 
  
  cmp     eax,   ' '   
  mov     edx,   FCM_MASK_SPACE_OR_NEG
  cmove   ecx,   edx
  
  cmp     eax,   '+'   
  mov     edx,   FCM_MASK_SIGN_BIT
  cmove   ecx,   edx
  
  cmp     eax,   '#'   
  mov     edx,   FCM_MASK_OUTPUT_PREFIX
  cmove   ecx,   edx
  
  cmp     eax,   '-'   
  mov     edx,   FCM_MASK_LEFT_ALIGN
  cmove   ecx,   edx
  
  cmp     eax,   '0'   
  mov     edx,   FCM_MASK_ZEROPAD
  cmove   ecx,   edx 
  
  test    ecx,   ecx 
  je      @width 
  
  or      dword ptr $stack (stk_push, VAR_FLAGS),  ecx 
  add     esi,   1
  
  jmp     @flags
  
@width:
  ;; -----------------------------------------------------------------------
  ;; Collect width. 
  ;; -----------------------------------------------------------------------
  ;; number:
  ;; *: User-specified
  ;; -----------------------------------------------------------------------
  movzx   eax,   byte ptr[esi] ;; Fetch character
  
  test    eax,   eax  ;; Is null??
  je      @exit 

  cmp     eax,   '*'  ;; Is user-specified ????
  je      @wspec 
  
  $ccall  xxxatoi, esi 
  test   ecx,    ecx 
  je      @precision
  
  mov     dword ptr $stack (stk_push, VAR_WIDTH), eax 
  add    esi,    ecx 
  jmp     @precision
  
@wspec:
  mov     eax,   [ebx] ;;Fetch va_args 
  add     esi,   1
  
  mov     dword ptr $stack (stk_push, VAR_WIDTH), eax 
  add     ebx,   4 
  
  ;; -----------------------------------------------------------------------
  ;; Collect precision. 
  ;; -----------------------------------------------------------------------
  ;; .number:
  ;; .*: User-specified
  ;; -----------------------------------------------------------------------
@precision:
  movzx   eax,   byte ptr[esi] ;; Fetch character

  test    eax,   eax  ;; Is null??
  je      @exit 
  
  cmp     eax,   '.' 
  jne     @length
  
  or      dword ptr $stack (stk_push, VAR_FLAGS), FSC_MASK_FORMAT_FZERO 

  add     esi,   1
  
  movzx   eax,   byte ptr[esi] ;; Fetch character
  
  cmp     eax,   '*'  ;; Is user-specified ????
  je      @pspec 
  
  $ccall  xxxatoi, esi 
  test   ecx,    ecx 
  je      @length
  
  mov     dword ptr $stack (stk_push, VAR_PRECISION), eax 
  add    esi,    ecx 
  jmp     @length
  
@pspec:
  mov     eax,   [ebx] ;;Fetch va_args 
  add     esi,   1
  
  mov     dword ptr $stack (stk_push, VAR_PRECISION), eax 
  add     ebx,   4 
  
@length:

  fidrec    struct
    chsig    dword   ?
    mainid   dword   ?
    subid    dword   ?
    unused   dword   ?
  fidrec    ends

  ;; -----------------------------------------------------------------------
  ;; Collect length. 
  ;; -----------------------------------------------------------------------
  comment $
  --------------------------------------------------------------------------------------------------------------
          |                          specifiers
  --------------------------------------------------------------------------------------------------------------
  length  |  d i            |   u o x X                | f F e E g G a A |   c     |    s      |    p     |    n               
  --------------------------------------------------------------------------------------------------------------
  (none) |  int           |   unsigned int           |     double      |  int    |  char*    |  void*  |    int*           
  --------------------------------------------------------------------------------------------------------------
  hh      |  signed char    |   unsigned char          |                 |         |           |          |    signed char*    
  --------------------------------------------------------------------------------------------------------------
  h       |  short int     |   unsigned short int     |                 |         |           |          |    short int*     
  --------------------------------------------------------------------------------------------------------------
  l       |  long int      |   unsigned long int      |                 |  wint_t |  wchar_t* |          |    long int*      
  --------------------------------------------------------------------------------------------------------------
  ll      |  long long int |   unsigned long long int |                 |         |           |          |    long long int* 
  --------------------------------------------------------------------------------------------------------------
  j       |  intmax_t      |    uintmax_t              |                 |         |           |          |    intmax_t*      
  --------------------------------------------------------------------------------------------------------------
  z       |  size_t         |   size_t                 |                 |         |           |          |    size_t*         
  --------------------------------------------------------------------------------------------------------------
  t       |  ptrdiff_t      |   ptrdiff_t              |                 |         |           |          |    ptrdiff_t*      
  --------------------------------------------------------------------------------------------------------------
  L       |                 |                          |   long double   |         |           |          |                    
  --------------------------------------------------------------------------------------------------------------
  $
  
  .const 
  
make_len_lut macro symbol, s_default, s_hh, s_h, s_l, s_ll, s_j, s_z, s_t, sL 

  symbol dd s_default, s_hh, s_h, s_l, s_ll, s_j, s_z, s_t, sL

endm 

  make_len_lut lut_di,       INT_LENGTH, CHAR_LENGTH, SHORT_LENGTH, LONG_INT_LENGTH, LONG_LONG_INT_LENGTH, INT_MAX_LENGTH, SIZE_MAX_LENGTH, PTRDIFF_LENGTH, INT_LENGTH
  make_len_lut lut_uoxX,     INT_LENGTH, CHAR_LENGTH, SHORT_LENGTH, LONG_INT_LENGTH, LONG_LONG_INT_LENGTH, INT_MAX_LENGTH, SIZE_MAX_LENGTH, PTRDIFF_LENGTH, INT_LENGTH
  make_len_lut lut_fFeEgGaA, DOUBLE_LENGTH, DOUBLE_LENGTH, DOUBLE_LENGTH, DOUBLE_LENGTH, DOUBLE_LENGTH, DOUBLE_LENGTH, DOUBLE_LENGTH, DOUBLE_LENGTH, LONG_DOUBLE_LENGTH
  make_len_lut lut_c,        CHAR_LENGTH, CHAR_LENGTH, CHAR_LENGTH, WCHAR_LENGTH, CHAR_LENGTH, CHAR_LENGTH, CHAR_LENGTH, CHAR_LENGTH, CHAR_LENGTH
  make_len_lut lut_sS,        PTR_LENGTH, PTR_LENGTH, PTR_LENGTH, PTR_LENGTH, PTR_LENGTH, PTR_LENGTH, PTR_LENGTH, PTR_LENGTH, PTR_LENGTH
  make_len_lut lut_p,        PTR_LENGTH, PTR_LENGTH, PTR_LENGTH, PTR_LENGTH, PTR_LENGTH, PTR_LENGTH, PTR_LENGTH, PTR_LENGTH, PTR_LENGTH
  make_len_lut lut_n,        INT_LENGTH, CHAR_LENGTH, SHORT_LENGTH, LONG_INT_LENGTH, LONG_LONG_INT_LENGTH, INT_MAX_LENGTH, SIZE_MAX_LENGTH, PTRDIFF_LENGTH, INT_LENGTH
  
  ;; Make format string id infos
  fidrecI fidrec <'d', 0, 0, 0>, <'i', 0, 1, 0>, \
                 <'u', 1, 0, 0>, <'o', 1, 1, 0>, <'x', 1, 2, 0>, <'X', 1, 3, 0>, \
                 <'f', 2, 0, 0>, <'F', 2, 1, 0>, <'e', 2, 2, 0>, <'E', 2, 3, 0>, <'g', 2, 4, 0>, <'G', 2, 5, 0>, <'a', 2, 6, 0>, <'A', 2, 7, 0>, \
                 <'c', 3, 0, 0>, \
                 <'s', 4, 0, 0>, <'S', 4, 1, 0>, \
                 <'p', 5, 0, 0>, \
                 <'n', 6, 0, 0>   
  fmproc  dd @di, @uoxX, @fFeEgGaA, @c, @sS, @p, @n
  
  .code 
  
  movzx   eax,   byte ptr[esi] ;; Fetch character
  
  ;; 1 hh 
  ;; 2 h   
  ;; 3 l   
  ;; 4 ll
  ;; 5 j 
  ;; 6 z 
  ;; 7 t     
  ;; 8 L 
  
cmpset macro ch_, index_
  cmp     eax,   ch_
  mov     edx,   index_ 
  cmove   ecx,   edx 
endm 

  xor     ecx,   ecx 
  
  cmpset  'h', 2 
  cmpset  'l', 3  
  cmpset  'j', 5 
  cmpset  'z', 6  
  cmpset  't', 7 
  cmpset  'L', 8 

  test    ecx,   ecx 
  je      @type 
  
  add     esi,   1 
  mov     $stack(stk_push, VAR_LENGTH), ecx 
  
  mov     eax,   ecx 
  and     eax,   -2 
  
  cmp     eax,   2 
  jne     @type
  
  mov     ch,   [esi] ;; Fetch character
  
  ;;       cl ==2 && ch == 'h' then := 1
  ;;       cl ==3 && ch == 'l' then := 4
  xor     edx,  edx 
  cmp     ecx,   (('h' shl 8) + 2)
  mov     eax,  1 
  cmove   edx,  eax 
  
  cmp     ecx,   (('l' shl 8) + 3)
  mov     eax,  4 
  cmove   edx,  eax  
  
  test    edx,  edx 
  je      @type
  
  add     esi,   1 
  mov     $stack (stk_push, VAR_LENGTH), edx
  
@type:

  movzx   eax,   byte ptr[esi] ;; Fetch character
  cmp     eax,   '%' 
  
  je      @percent
  
  ;; Serach type character
  assume  ebp: ptr fidrec
  
  xor     edx,   edx 
  xor     ecx,   ecx 
  
  lea     ebp,   fidrecI
  mov     ecx,   (sizeof fidrecI) / (sizeof fidrec)

@@:
  cmp     [ebp].chsig, eax 
  je      @F

  add     ebp,   sizeof fidrec
  
  sub     ecx,   1
  jne     @B
 
@@:
  test    ecx,   ecx 
  je      @formatloop
  
  add     esi,   1 
  
  mov     eax,   [ebp].mainid 
  jmp     dword ptr fmproc[eax * 4]
  
@u: ;; %u 
  or      dword ptr $stack (stk_push, VAR_FLAGS), FSC_MASK_FORMAT_U
@di: ;; %d %i 
  mov     ecx,   $stack (stk_push, VAR_PRECISION)
  cmp     ecx,   0
  mov     eax,   0 
  cmovl   ecx,   eax 
  mov     $stack (stk_push, VAR_PRECISION), ecx 
  
  ;;       Fetch  length 
  mov     edx,   $stack (stk_push, VAR_LENGTH) 
  mov     edx,   lut_di[edx * 4]
  
  mov     eax,   edx 
  shr     eax,   6 
  
  stk_push_n = stk_push + $push ([ebx + 4], [ebx], edx, ecx)
  
  lea     ebx,  [ebx + 4 + eax * 4]
  
  mov     eax, $stack (stk_push_n, VAR_WIDTH)
  mov     ecx, $stack (stk_push_n, VAR_FLAGS) 

  stk_push_n = stk_push_n + $push (eax, ecx, edi)
  
  test    ecx, FSC_MASK_FORMAT_U 
  jne     @F 
  
  call    format$d
  add     esp, (stk_push_n - stk_push)
  add     edi, eax 
  jmp     @formatloop
  
@@:
  call    format$u 
  add     esp, (stk_push_n - stk_push)
  add     edi, eax 
  jmp     @formatloop

@uoxX: 
  mov     eax, [ebp].subid
  jmp     dword ptr[@lsu_lut + eax *4]
  
  .const 
  
  @lsu_lut dd @u, @o, @x, @X 
  
  .code 
  
@o: 
  .const 
  
  @o_prefix db "0", 0
  
  .code 
     
  mov     ecx,   $stack (stk_push, VAR_PRECISION)
  cmp     ecx,   0
  mov     eax,   0 
  cmovl   ecx,   eax 
  mov     $stack (stk_push, VAR_PRECISION), ecx 
  
  ;;       Fetch  length 
  mov     edx,   $stack (stk_push, VAR_LENGTH) 
  mov     edx,   lut_uoxX[edx * 4]
  
  mov     eax,   edx 
  shr     eax,   6 
  
  stk_push_n = stk_push + $push (offset @o_prefix, 0, 8, [ebx + 4], [ebx], edx, ecx)
  
  lea     ebx,  [ebx + 4 + eax * 4]
  
  mov     eax, $stack (stk_push_n, VAR_WIDTH)
  mov     ecx, $stack (stk_push_n, VAR_FLAGS) 

  stk_push_n = stk_push_n + $push (eax, ecx, edi)
  
  call    format$x_$o_$b
  add     esp, (stk_push_n - stk_push)
  add     edi, eax 
  jmp     @formatloop
  
@p:
  mov     dword ptr $stack (stk_push, VAR_LENGTH), 7  ;;  ptrdiff_t
@x:
  .const 
  
  @x_prefix db "0x", 0
  
  .code 
  
  mov     ecx,   $stack (stk_push, VAR_PRECISION)
  cmp     ecx,   0
  mov     eax,   0 
  cmovl   ecx,   eax 
  mov     $stack (stk_push, VAR_PRECISION), ecx 
  
  ;;       Fetch  length 
  mov     edx,   $stack (stk_push, VAR_LENGTH) 
  mov     edx,   lut_uoxX[edx * 4]
  
  mov     eax,   edx 
  shr     eax,   6 
  
  stk_push_n = stk_push + $push (offset @x_prefix, 0, 16, [ebx + 4], [ebx], edx, ecx)
  
  lea     ebx,  [ebx + 4 + eax * 4]
  
  mov     eax, $stack (stk_push_n, VAR_WIDTH)
  mov     ecx, $stack (stk_push_n, VAR_FLAGS) 

  stk_push_n = stk_push_n + $push (eax, ecx, edi)
  
  call    format$x_$o_$b
  add     esp, (stk_push_n - stk_push)
  add     edi, eax 
  jmp     @formatloop
  
@X:
  .const 
  
  @X_prefix db "0X", 0
  
  .code 

  mov     ecx,   $stack (stk_push, VAR_PRECISION)
  cmp     ecx,   0
  mov     eax,   0 
  cmovl   ecx,   eax 
  mov     $stack (stk_push, VAR_PRECISION), ecx 
  
  ;;       Fetch  length 
  mov     edx,   $stack (stk_push, VAR_LENGTH) 
  mov     edx,   lut_uoxX[edx * 4]
  
  mov     eax,   edx 
  shr     eax,   6 
  
  stk_push_n = stk_push + $push (offset @X_prefix, 1, 16, [ebx + 4], [ebx], edx, ecx)
  
  lea     ebx,  [ebx + 4 + eax * 4]
  
  mov     eax, $stack (stk_push_n, VAR_WIDTH)
  mov     ecx, $stack (stk_push_n, VAR_FLAGS) 

  stk_push_n = stk_push_n + $push (eax, ecx, edi)
  
  call    format$x_$o_$b
  add     esp, (stk_push_n - stk_push)
  add     edi, eax 
  jmp     @formatloop

@fFeEgGaA:
  mov     eax, [ebp].subid
  jmp     dword ptr[@lfu_lut + eax *4]

  .const 
  
  @lfu_lut dd $f, $F, $e, $E, $g, $G, $a, $A
  
  .code 
$f:
$F:
  mov     ecx,   $stack (stk_push, VAR_PRECISION)
  cmp     ecx,   0 
  
  jge      @F 
  
  mov     ecx,   6
  xor     eax,   eax 
  test    dword ptr $stack (stk_push, VAR_FLAGS), FSC_MASK_FORMAT_FZERO
  cmovne  ecx,   eax 
@@:
  ;;       Fetch  length 
  mov     edx,   $stack (stk_push, VAR_LENGTH) 
  mov     edx,   lut_fFeEgGaA[edx * 4]
  
  mov     eax,   edx 
  shr     eax,   6 
  
  stk_push_n = stk_push + $push (0, 0, [ebp].subid, 0, [ebx + 4], [ebx], edx, ecx)
  
  lea     ebx,  [ebx + 4 + eax * 4]
  
  mov     eax, $stack (stk_push_n, VAR_WIDTH)
  mov     ecx, $stack (stk_push_n, VAR_FLAGS) 

  stk_push_n = stk_push_n + $push (eax, ecx, edi)
  
  call    format$f
  add     esp, (stk_push_n - stk_push)
  add     edi, eax 
  jmp     @formatloop
 
$e: 
$E: 
  mov     ecx,   $stack (stk_push, VAR_PRECISION)
  cmp     ecx,   0 
  
  jge      @F 
  
  mov     ecx,   6
  xor     eax,   eax 
  test    dword ptr $stack (stk_push, VAR_FLAGS), FSC_MASK_FORMAT_FZERO
  cmovne  ecx,   eax 
@@:
  ;;       Fetch  length 
  mov     edx,   $stack (stk_push, VAR_LENGTH) 
  mov     edx,   lut_fFeEgGaA[edx * 4]
  
  mov     eax,   edx 
  shr     eax,   6 
  
  stk_push_n = stk_push + $push  (esi)
   
  mov     esi,   [ebp].subid
  and     esi,   1       ;;XXX: Order depend
  
  stk_push_n = stk_push_n + $push (0, 0, 0, esi, 0, [ebx + 4], [ebx], edx, ecx)
  
  lea     ebx,  [ebx + 4 + eax * 4]
  
  mov     eax, $stack (stk_push_n, VAR_WIDTH)
  mov     ecx, $stack (stk_push_n, VAR_FLAGS) 

  stk_push_n = stk_push_n + $push (eax, ecx, edi)
  
  call    format$e

  add     esp, (stk_push_n - stk_push - 4)

  pop     esi

  add     edi, eax 
  jmp     @formatloop
  
$g: ;; TODO:
$G: ;; TODO:
$a: ;; TODO:
$A: ;; TODO:

@c:   ;; %c 
  mov     ecx,   $stack (stk_push, VAR_PRECISION)
  cmp     ecx,   0
  mov     eax,   not (1 shl 31) 
  cmovl   ecx,   eax 
  
  mov     edx,   $stack (stk_push, VAR_LENGTH) 
  
  stk_push_n = stk_push + $push ([ebx + 0], lut_c[edx * 4], ecx)
  
  mov     eax, $stack (stk_push_n, VAR_WIDTH)
  mov     ecx, $stack (stk_push_n, VAR_FLAGS) 

  stk_push_n = stk_push_n + $push (eax, ecx, edi)
  
  call    format$c
  add     esp, (stk_push_n - stk_push)
  
  add     edi, eax 
  add     ebx, 4 
  jmp     @formatloop
  
@sS: ;; %s %S 
  mov     ecx,   $stack (stk_push, VAR_PRECISION)
  cmp     ecx,   0
  mov     eax,   not (1 shl 31) 
  cmovl   ecx,   eax 
  
  mov     edx,   $stack (stk_push, VAR_LENGTH) 
  
  stk_push_n = stk_push + $push ([ebp].subid, [ebx + 0], lut_sS[edx * 4], ecx)
  
  mov     eax, $stack (stk_push_n, VAR_WIDTH)
  mov     ecx, $stack (stk_push_n, VAR_FLAGS) 

  stk_push_n = stk_push_n + $push (eax, ecx, edi)
  
  call    format$s
  add     esp, (stk_push_n - stk_push)
  
  add     edi, eax 
  add     ebx, 4 
  jmp     @formatloop

@n: ;; %n 
  mov     ecx,   $stack (stk_push, ARG_BUFFER)
  mov     eax,   edi 
  sub     eax,   ecx 
  
  mov     edx,   $stack (stk_push, VAR_LENGTH) 
  mov     edx,   lut_n[edx * 4]
  
  shr     edx,   3  
  bsf     edx,   edx 
  jmp     dword ptr @lsn_lut[edx * 4]
  
@lsn8:
  mov     edx, [ebx]
  mov     [edx], al 
  add     ebx,   4 
  jmp     @formatloop
 
@lsn16:
  mov     edx, [ebx]
  mov     [edx], ax 
  add     ebx,   4 
  jmp     @formatloop

@lsn32:
  mov     edx, [ebx]
  mov     [edx], eax 
  add     ebx,   4 
  jmp     @formatloop

@lsn64:
  mov     edx, [ebx]
  mov     [edx], eax 
  xor     eax, eax 
  mov     [edx+4], eax
  add     ebx,   4 
  jmp     @formatloop
  
  .const 
  
  @lsn_lut dd @lsn8, @lsn16, @lsn32, @lsn64 
  
  .code 
  
@percent:
  mov     byte ptr[edi], '%' 
  
  add     esi,    1
  add     edi,    1
  
  jmp     @formatloop
  
_rtl_vsprintf endp

  end 
